package com.example.myboot01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MulcamBoot1Application {

	public static void main(String[] args) {
		SpringApplication.run(MulcamBoot1Application.class, args);
	}

}
