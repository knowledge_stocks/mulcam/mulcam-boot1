package com.example.myboot01;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DemoController {
  
  @RequestMapping("/")
  @ResponseBody
  public String home() {
    System.out.println("Hello Boot!!");
    return "Hello Boot!!";
  }
  
  @RequestMapping("/hello/hello")
  public String hello(Model model, HttpServletRequest request) {
    System.out.println("model");
    model.addAttribute("message", "hello.jsp입니다.");
    return "hello";
  }
}
